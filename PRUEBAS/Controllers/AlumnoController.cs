﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MODELO;
using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace PRUEBAS.Controllers
{
    public class AlumnoController : Controller
    {
        PruebasEntities pe = new PruebasEntities();
        // GET: Alumno
        public ActionResult ListaAlumnos()
        {
            return View(pe.sp_listaAlumnos().ToList());
        }



        //INICIO DE ACCION PARA EXPORTAR REPORTE SIN FILTRO
        public ActionResult ExportReport()
        {
            //List allEverest = new List();

            List<sp_listaAlumnos_Result> allEverest = new List<sp_listaAlumnos_Result>();

                allEverest = pe.sp_listaAlumnos().ToList();

            

            ReportDocument rd = new ReportDocument();
            
            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), "ListaAlumno.rpt"));
            rd.SetDataSource(allEverest);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            try
            {
                //=====================Inicio para Exportar a PDF============================================
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "EverestList.pdf");
                //=====================Fin para Exportar a PDF===============================================

                //=====================Inicio para Exportar a Excel==========================================
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/xls", "EverestList.xls");
                //=====================Fin para Exportar a Excel=============================================

                //=====================Inicio para Exportar a Word===========================================
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/doc", "EverestList.doc");
                //=====================Fin para Exportar a Word==============================================
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //FIN DE ACCION PARA EXPORTAR REPORTE SIN FILTRO

        //INICIO DE ACCION PARA EXPORTAR REPORTE CON 1 FILTRO
        public ActionResult ReporteParametros(int? idPais)
        {
            List<sp_ReportePorPais_Result> allEverest = new List<sp_ReportePorPais_Result>();

            allEverest = pe.sp_ReportePorPais(idPais).ToList();

            ReportDocument rd = new ReportDocument();

            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), "ListaAlumno.rpt"));
            rd.SetDataSource(allEverest);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            try
            {
                //=====================Inicio para Exportar a PDF============================================
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "EverestList.pdf");
                //=====================Fin para Exportar a PDF===============================================

                //=====================Inicio para Exportar a Excel==========================================
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/xls", "EverestList.xls");
                //=====================Fin para Exportar a Excel=============================================

                //=====================Inicio para Exportar a Word===========================================
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/doc", "EverestList.doc");
                //=====================Fin para Exportar a Word==============================================
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //FIN DE ACCION PARA EXPORTAR REPORTE CON 1 FILTRO

        //INICIO DE ACCION PARA EXPORTAR REPORTE CON 2 FILTROS
        public ActionResult ReporteParametros2(string Codigo, int? idPais)
        {
            List<sp_ReportePorPaisYcodigo_Result> allEverest = new List<sp_ReportePorPaisYcodigo_Result>();

            allEverest = pe.sp_ReportePorPaisYcodigo(Codigo, idPais).ToList();

            ReportDocument rd = new ReportDocument();

            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), "ListaAlumno.rpt"));
            rd.SetDataSource(allEverest);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            try
            {
                //=====================Inicio para Exportar a PDF============================================
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "EverestList.pdf");
                //=====================Fin para Exportar a PDF===============================================

                //=====================Inicio para Exportar a Excel==========================================
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/xls", "EverestList.xls");
                //=====================Fin para Exportar a Excel=============================================

                //=====================Inicio para Exportar a Word===========================================
                //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.WordForWindows);
                //stream.Seek(0, SeekOrigin.Begin);
                //return File(stream, "application/doc", "EverestList.doc");
                //=====================Fin para Exportar a Word==============================================
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //FIN DE ACCION PARA EXPORTAR REPORTE CON 2 FILTROS
    }



}